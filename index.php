<?php
error_reporting(0);
?>
<!DOCTYPE html>
<html lang="en-US">
<title>Files Uploader</title>
<meta charset="utf-8">
<meta name="Keywords" content="Image upload in php and ajax with progress bar">
<meta name="Description" content="Image upload in php and ajax with progress bar">
<body>
<div style="margin:0 auto;">
<div style="border:1px solid; padding:10px 10px 10px 10px; width:90%; float:left;" >
<a href='' style="float:right">Reload</a><br/>
<form enctype="multipart/form-data" id="myform">  
   <b>Choose file to upload:</b><br/><br/>
    <input type="file"  name="fileToUpload" id="image" /> 
    <input type="button" value="Upload image" class="uploadBtn" />
</form>
<div id="progBar" style="display:none;">
  <progress value="0" max="100" style="width:750px;"></progress><span id="prog" style="font-weight:bold;">0%</span>
</div>
<div id="alertMsg" style="font-size:16px; color:blue; display:none;"></div>
<h3>List of uploaded files:</h3>
<table border=1 width=100% id="tb" align="left">
<tr style="display:none;"><td colspan=2 ></td></tr>
<?php 
  $dir = "files/"; 
  $i=0;
    foreach(glob($dir."{*.*}", GLOB_BRACE) as $file){ 
             echo "<tr id='row".$i."'><td>".$file."</td><td><a href='javascript:void(0);' id='delete' rmid='row".$i."' filename='".$file."'>Delete</a></td></tr>";
             $i++;    
}
?>
   </table>
  </div>
 </div>
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <script src="js/upload.js"></script>
 </body>
</html>
